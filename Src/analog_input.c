/*
 * analog_input.c
 *
 *  Created on: Nov 20, 2018
 *      Author: zamek
 */

#include <main.h>
#include <analog_input.h>
#include <stdbool.h>

#include <FreeRTOS.h>
#include <semphr.h>
#include <cmsis_os.h>
#include <string.h>
#include <stm32f4xx.h>

#define AD_EVT_CONV_END  (1<<0)

#define AD_EVT_CONV_START (1<<1)

#define AD_EVT_CONV_STOP (1<<2)

#define AD_EVT_MASK ((AD_EVT_CONV_START) | (AD_EVT_CONV_STOP) | (AD_EVT_CONV_END))

extern ADC_HandleTypeDef hadc1;
extern TaskHandle_t TaskADHandle;

typedef struct {
	ai_analog_value_t raw_value;
	ai_analog_value_t queue[ANALOG_MOVING_QUEUE_SIZE];
	uint8_t queue_index;
	ai_analog_value_t current_value;
} moving_average_t;

typedef struct {
	ai_analog_value_t delta;
	ai_analog_value_t hyst_max;
	ai_analog_value_t hyst_min;
} moving_hysteresis_t;

typedef struct {
	ai_analog_value_t raw_values[ANALOG_CHANNELS];
	ai_analog_value_t normalized_values[ANALOG_CHANNELS];
	moving_average_t moving_average[ANALOG_CHANNELS];
	moving_hysteresis_t moving_hysteresis[ANALOG_CHANNELS];
	bool initialized;
	SemaphoreHandle_t semaphore;
	uint32_t ad_conversion_errors;
	uint32_t ad_conversion_success;
	uint32_t ad_start_errors;
	uint32_t ad_stop_errors;
	bool running;
} analog_input_t;

static analog_input_t analog_input;

enum AI_ERRORS  ai_init(){
	analog_input.initialized = false;
	bzero(analog_input.moving_average, sizeof(analog_input.moving_average));
	bzero(analog_input.moving_hysteresis, sizeof(analog_input.moving_hysteresis));
	for(int i=0;i<ANALOG_CHANNELS;++i)
		analog_input.moving_hysteresis[i].delta = ANALOG_HYSTERESIS_DELTA;

	analog_input.semaphore = xSemaphoreCreateMutex();
	configASSERT(analog_input.semaphore);

	analog_input.initialized = true;
	return AI_OK;
}

enum AI_ERRORS  ai_deinit(){
	analog_input.initialized = false;
	return AI_OK;
}

static void moving_average() {
	for(int i=0;i<ANALOG_CHANNELS;++i)
		analog_input.normalized_values[i]=analog_input.raw_values[i];

	//TODO
}

static void moving_hysteresis() {
//TODO
}

static void start_conversion() {

	BaseType_t result = HAL_ADC_Start_DMA(&hadc1, analog_input.raw_values,
										  ANALOG_CHANNELS);
	if (result != HAL_OK)
		++analog_input.ad_start_errors;
	else
		analog_input.running = true;
}

void ai_process(){
	uint32_t notified_value;
	BaseType_t result;

	for(;;) {
		if (analog_input.initialized) {

			start_conversion();

			xTaskNotifyWait(AD_EVT_MASK, AD_EVT_MASK, &notified_value,
							pdMS_TO_TICKS(ANALOG_NOTIFY_WAIT_MS));

			if (notified_value & AD_EVT_CONV_STOP) {
				analog_input.running = false;
				result = HAL_ADC_Stop_DMA(&hadc1);
				if (result!=HAL_OK)
					++analog_input.ad_stop_errors;

				goto wait;
			}

			if (notified_value & AD_EVT_CONV_START) {
				start_conversion();
				goto wait;
			}

			if (notified_value & AD_EVT_CONV_END) {
				if (xSemaphoreTake(analog_input.semaphore, (TickType_t) 10) == pdTRUE) {
					++analog_input.ad_conversion_success;
					moving_average();
					moving_hysteresis();
					xSemaphoreGive(analog_input.semaphore);
				}//xSemaphoreTake(
			}

		} //analog_input.initialized

wait:
		osDelay(ANALOG_TASK_DELAY_MS);
	} //for
}

enum AI_ERRORS ai_get_values(ai_analog_value_t *values, uint8_t size){
	configASSERT(values);
	assert_param(size);

	if (size!=ANALOG_CHANNELS)
		return AI_BAD_PARAM;

	if (!analog_input.initialized)
		return AI_UNINITIALIZED;

	if (xSemaphoreTake(analog_input.semaphore, (TickType_t) 10) == pdTRUE) {
		memcpy(values, analog_input.normalized_values,
				sizeof(analog_input.normalized_values));
		xSemaphoreGive(analog_input.semaphore);
		return AI_OK;
	}
	return AI_SEMAPHORE_BUSY;
}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc) {
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	xTaskNotifyFromISR(TaskADHandle, AD_EVT_CONV_END, eSetBits,
						&xHigherPriorityTaskWoken);
}

void HAL_ADC_ErrorCallback(ADC_HandleTypeDef *hadc) {
	HAL_ADC_Stop_DMA(&hadc1);
	++analog_input.ad_conversion_errors;
}

static const char *CMD_MOVING_AVERAGE = "ma";
static const char *CMD_MOVING_HYSTERESIS = "mh";

static void cmd_show(int channel) {
	sh_printf("channel %d.: raw:%d, normalized:%d\r\n", channel,
				analog_input.raw_values[channel], analog_input.normalized_values[channel]);
}

static void usage() {
	sh_printf("usage %s <ma|mh>\r\n", CMD_AD);
	sh_printf("where ma is moving average and mh is moving hysteresis\r\n");
}

static void cmd_show_moving_average() {
	sh_printf("Moving average values by channel");
	for (int i=0;i<ANALOG_CHANNELS;++i) {
		sh_printf("channel %d.: raw:%d current:%d, index:%d\r\n",
				  i, analog_input.moving_average[i].raw_value, analog_input.moving_average[i].current_value);
		sh_printf("queue is:");
		for (int j=0;j<ANALOG_MOVING_QUEUE_SIZE;j++)
			sh_printf("%d ", analog_input.moving_average[i].queue[j]);

		sh_printf("\r\n");
	}
}

static void cmd_show_moving_hysteresis() {
	sh_printf("Moving hysteresis values by channel");
	for (int i=0;i<ANALOG_CHANNELS;++i) {
		sh_printf("channel %d.: hyst min:%d hyst max:%d, delta:%d\r\n",
				  i, analog_input.moving_hysteresis[i].hyst_min,
				     analog_input.moving_hysteresis[i].hyst_max,
					 analog_input.moving_hysteresis[i].delta);
	}
}

void ai_cmd_ad(int argc, char *argv[]) {
	if (argc==0) {
		for (int i=0;i<ANALOG_CHANNELS;++i)
			cmd_show(i);

		sh_printf("running:%s c.success:%d, c.errors:%d, start errors:%d, stop errors:%d \r\n",
				analog_input.running ? "true" : "false",
				analog_input.ad_conversion_success,
				analog_input.ad_conversion_errors,
				analog_input.ad_start_errors,
				analog_input.ad_stop_errors);
		return;
	}

	if (argc==1) {
		if (strcmp(CMD_MOVING_AVERAGE, argv[0])==0) {
			cmd_show_moving_average();
			return;
		}

		if (strcmp(CMD_MOVING_HYSTERESIS, argv[0])==0) {
			cmd_show_moving_hysteresis();
			return;
		}
		usage();
		return;
	}

	usage();
}
