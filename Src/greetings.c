/*
 * greetings.c
 *
 *  Created on: Nov 13, 2018
 *      Author: zamek
 */

#include <greetings.h>


void cmd_greetings(int argc, char *argv[]) {
	if (argc==0) {
		sh_printf("Usage: greet <name>");
		return;
	}

	sh_printf("hello %s\r\n", argv[0]);
}


