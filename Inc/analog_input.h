/*
 * analog_input.h
 *
 *  Created on: Nov 20, 2018
 *      Author: zamek
 */

#ifndef ANALOG_INPUT_H_
#define ANALOG_INPUT_H_

#include <stdint.h>
#include <shell.h>
#include <stm32f4xx_hal.h>
#include <stm32f4xx_hal_adc.h>
#include <main.h>

enum AI_ERRORS {
		AI_OK=0,
		AI_BAD_PARAM=-1,
		AI_SEMAPHORE_BUSY=-2,
		AI_UNINITIALIZED=-3
};

#define CMD_AD "ad"

#define ANALOG_COMMANDS \
		{ .command_name =CMD_AD, .function = ai_cmd_ad }

typedef uint32_t ai_analog_value_t;

enum AI_ERRORS ai_init();

enum AI_ERRORS ai_deinit();

void ai_process();

enum AI_ERRORS ai_get_values(ai_analog_value_t *values, uint8_t size);

void ai_cmd_ad(int argc, char *argv[]);

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc);

void HAL_ADC_ErrorCallback(ADC_HandleTypeDef *hadc);

#endif /* ANALOG_INPUT_H_ */
