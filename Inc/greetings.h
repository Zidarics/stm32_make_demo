/*
 * greetings.h
 *
 *  Created on: Nov 13, 2018
 *      Author: zamek
 */

#ifndef GREETINGS_H_
#define GREETINGS_H_

#include <shell.h>

void cmd_greetings(int argc, char *argv[]);

#define GREETING_COMMANDS \
	{ .command_name ="greet", .function = cmd_greetings }


#endif /* GREETINGS_H_ */
